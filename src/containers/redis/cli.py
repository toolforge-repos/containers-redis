# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Toolforge Redis container.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
import logging
import os
import pathlib
import sys

import click
import coloredlogs

from . import settings
from . import utils
from .version import __version__

logger = logging.getLogger(__name__)


@click.group()
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase debug logging verbosity",
)
@click.pass_context
def main(ctx, verbose):
    """Setup and run redis."""

    coloredlogs.install(
        level=max(logging.DEBUG, logging.WARNING - (10 * verbose)),
        fmt="%(asctime)s %(name)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES
        | {
            "debug": {},
            "info": {"color": "green"},
        },
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES
        | {
            "asctime": {"color": "yellow"},
        },
    )
    logging.captureWarnings(True)

    ctx.obj = {
        "REDIS_PASSWORD": settings.REDIS_PASSWORD,
    }


@main.command()
@click.pass_context
def redis(ctx):
    """Run Redis"""
    logger.info("Generating config")
    config_dir = pathlib.Path.cwd() / ".config"

    utils.generate_config(
        config_dir / "redis.conf",
        "redis.conf",
        ctx.obj,
    )

    logger.info("Starting redis")
    sys.stdout.flush()
    sys.stderr.flush()
    os.execlp(
        "redis-server",
        "redis-server",
        config_dir / "redis.conf",
    )


@main.command(context_settings={"ignore_unknown_options": True})
@click.argument("extra_args", nargs=-1, type=click.UNPROCESSED)
def rediscli(extra_args):
    """Run redis-cli"""
    os.environ["REDISCLI_AUTH"] = settings.REDIS_PASSWORD

    logger.info("Starting redis-cli")
    sys.stdout.flush()
    sys.stderr.flush()
    os.execlp(
        "redis-cli",
        "redis-cli",
        "-h",
        settings.REDIS_SERVICE_NAME,
        *extra_args,
    )


if __name__ == "__main__":  # pragma: nocover
    main()
