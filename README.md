Toolforge Redis container
=========================

[Build Service][] project creating a container running [Redis][].

Use with your tool
------------------
```
$ ssh dev.toolforge.org
$ become $TOOL
$ toolforge envvars create REDIS_PASSWORD
$ toolforge jobs run \
  --image tool-containers/redis:latest \
  --command server \
  --continuous \
  --emails none \
  --port 6379 \
  redis
```

The Redis server will be available to your other containers at`redis:6379`.
Clients will need to use the REDIS_PASSWORD envvar to authenticate to the
server.

Publish a new container
-----------------------
```
$ ssh dev.toolforge.org
$ become containers
$ toolforge build start --image-name redis \
  https://gitlab.wikimedia.org/toolforge-repos/containers-redis
```

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

[Build Service]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service
[Redis]: https://en.wikipedia.org/wiki/Redis
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.html
[COPYING]: COPYING
